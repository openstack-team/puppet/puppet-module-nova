puppet-module-nova (25.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Rebased / refreshed:
    - fix-libvirtd-config-for-tls.patch
    - use-modular-libvirt-in-debian-and-ubuntu.patch

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Oct 2024 11:13:58 +0200

puppet-module-nova (24.0.0-2) unstable; urgency=medium

  * Fix puppet-module-nova.install.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 May 2024 10:46:27 +0200

puppet-module-nova (24.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.
  * Rebased fix-libvirtd-config-for-tls.patch.
  * Rebased use-modular-libvirt-in-debian-and-ubuntu.patch.
  * Refreshed fix-no-virtproxyd-in-debian.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Apr 2024 17:04:06 +0200

puppet-module-nova (23.0.0-3) unstable; urgency=medium

  * Add fix-no-virtproxyd-in-debian.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 10 Nov 2023 09:53:26 +0100

puppet-module-nova (23.0.0-2) unstable; urgency=medium

  * Add use-modular-libvirt-in-debian-and-ubuntu.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Oct 2023 12:45:03 +0200

puppet-module-nova (23.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.
  * Removed remove-validate-legacy-check.patch, as it's not relevant because
    Debian uses puppet 7 now.
  * Fixed fix-libvirtd-config-for-tls.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Oct 2023 15:49:05 +0200

puppet-module-nova (22.0.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 11:26:12 +0200

puppet-module-nova (22.0.0-2) experimental; urgency=medium

  * Fix migration/libvirt.pp.

 -- Thomas Goirand <zigo@debian.org>  Wed, 12 Apr 2023 14:27:36 +0200

puppet-module-nova (22.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed runtime depends versions for this release.
  * Rebased / refreshed patches.

 -- Thomas Goirand <zigo@debian.org>  Wed, 12 Apr 2023 10:59:55 +0200

puppet-module-nova (21.0.0-2) unstable; urgency=medium

  * Add fix-libvirtd-config-for-tls.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Feb 2023 11:53:37 +0100

puppet-module-nova (21.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Refreshed patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 20 Oct 2022 23:24:01 +0200

puppet-module-nova (20.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Rebased remove-validate-legacy-check.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2022 12:28:22 +0200

puppet-module-nova (20.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 13:55:01 +0100

puppet-module-nova (20.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Removed fix-path-for-etc-default-libvirtd.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Mar 2022 23:06:57 +0100

puppet-module-nova (19.4.0-3) unstable; urgency=medium

  * Clean-up update-alternatives handling.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Jan 2022 10:18:03 +0100

puppet-module-nova (19.4.0-2) unstable; urgency=medium

  * Add fix-path-for-etc-default-libvirtd.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Dec 2021 21:46:06 +0100

puppet-module-nova (19.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Refreshed remove-validate-legacy-check.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Oct 2021 09:54:52 +0200

puppet-module-nova (19.3.0-1) unstable; urgency=medium

  * Uploading to unstable.
  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 15:01:12 +0200

puppet-module-nova (19.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Removed patch applied upstream:
    - Add_support_for_nova_api_uwsgi_config_in_Debian.patch
    - Debian-stop-tweaking-uwsgi_threads.patch
    - Switch-to-ini_setting.patch
  * Disable patch:
    - fix-libvirt-setup-under-bullseye-and-up.patch
  * Rebased:
    - remove-validate-legacy-check.patch

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Sep 2021 14:47:49 +0200

puppet-module-nova (18.4.0-4) unstable; urgency=medium

  * Add Add_support_for_nova_api_uwsgi_config_in_Debian.patch.
  * Add Debian-stop-tweaking-uwsgi_threads.patch.
  * Add Switch-to-ini_setting.patch.
  * Add remove-validate-legacy-check.patch.
  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 17:28:46 +0200

puppet-module-nova (18.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 06 Apr 2021 12:45:00 +0200

puppet-module-nova (18.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 21:47:11 +0100

puppet-module-nova (18.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Rebased fix-libvirt-setup-under-bullseye-and-up.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Mar 2021 17:54:06 +0100

puppet-module-nova (17.5.0-2) unstable; urgency=medium

  * Fix libvirt setup under >= Bullseye:
    - fix-libvirt-setup-under-bullseye-and-up.patch

 -- Thomas Goirand <zigo@debian.org>  Mon, 25 Jan 2021 23:36:51 +0100

puppet-module-nova (17.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 16:22:10 +0200

puppet-module-nova (17.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Oct 2020 13:59:26 +0200

puppet-module-nova (16.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 11:45:20 +0200

puppet-module-nova (16.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 03 May 2020 13:54:18 +0200

puppet-module-nova (16.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 26 Apr 2020 12:40:21 +0200

puppet-module-nova (15.4.0-3) unstable; urgency=medium

  * Depends on puppet, not puppet-common.
  * Standards-Version: 4.5.0.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Mar 2020 10:57:25 +0100

puppet-module-nova (15.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 00:31:14 +0200

puppet-module-nova (15.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed use-uwsgi-in-debian.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Oct 2019 11:08:50 +0200

puppet-module-nova (15.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Correct_virtlock_virtlogd_service_names_for_Debian.patch applied
    upstream.
  * Rebased use-uwsgi-in-debian.patch.

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Sep 2019 01:14:35 +0200

puppet-module-nova (14.4.0-4) unstable; urgency=medium

  * Switched build-dependencies to Python 3 (Closes: #937364).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 21:11:35 +0200

puppet-module-nova (14.4.0-3) unstable; urgency=medium

  * Add use-uwsgi-in-debian.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Sep 2019 16:43:24 +0200

puppet-module-nova (14.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 22:02:09 +0200

puppet-module-nova (14.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed runtime depends for this release.
  * Removed Fix-VNC-console-for-debian.patch applied upstream.
  * Refreshed Correct_virtlock_virtlogd_service_names_for_Debian.patch.
  * Removed fix-serial-console-proxy.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Apr 2019 10:44:17 +0200

puppet-module-nova (13.1.0-3) unstable; urgency=medium

  * Add fix-serial-console-proxy.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Feb 2019 10:53:02 +0100

puppet-module-nova (13.1.0-2) unstable; urgency=medium

  * Add Correct_virtlock_virtlogd_service_names_for_Debian.patch.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Nov 2018 16:58:31 +0100

puppet-module-nova (13.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Add VNC / SPICE console patch.
  * Fixed versions of depends: for this release.
  * Use team+openstack@tracker.debian.org for maintainer email.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jun 2018 16:24:08 +0200

puppet-module-nova (12.3.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Deprecating priority extra as per policy 4.0.1.

  [ Thomas Goirand ]
  * New upstraem release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Mar 2018 14:37:16 +0100

puppet-module-nova (9.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed runtime depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 21 Oct 2016 13:42:34 +0000

puppet-module-nova (8.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)

  [ Thomas Goirand ]
  * Added missing runtime dependencies.

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Apr 2016 10:29:52 -0500

puppet-module-nova (8.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Overrides dh_auto_install and dh_auto_build to avoid reno using git at
    build time.
  * Added python-pbr, python-setuptools and python-all as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Apr 2016 08:31:48 +0000

puppet-module-nova (8.0.0~b1-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2016 18:17:34 +0100

puppet-module-nova (7.0.0-1) unstable; urgency=medium

  * Initial release. (Closes: #814245)

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Feb 2016 16:55:51 +0800
